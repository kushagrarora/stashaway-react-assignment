import React, { PureComponent } from 'react';
import axios from 'axios';
import Ramen from './components/Ramen'
import InputGroup from './components/InputGroup'
import './App.css';


class App extends PureComponent{

  constructor(props){
    super(props);
    this.state = {
      isFetched : false,
      text : '',
      ramens : []
    }

    this.store = [];
    this.countries = [];
    this.filterByName = this.filterByName.bind(this);
    this.filterByYear = this.filterByYear.bind(this);
    this.filterByCountry = this.filterByCountry.bind(this);
  }

  componentDidMount(){
    axios.get(process.env.REACT_APP_API_URL)
    .then(res => {
      this.store = [...res.data];
      this.countries = this.store.map(ramen => ramen.Country);
      this.setState({
        isFetched : true,
        ramens : [...this.store]
      })
    })
    .catch(err => {
      console.log(err, err.response)
    })

  }

  filterByYear(year){
    let tempStore = [...this.store];

    tempStore = tempStore.map(ramen => {
      if(ramen['Top Ten'] !== "NaN"){
        const [year, rank] = ramen['Top Ten'].split(' #');
        ramen.year = parseInt(year);
        ramen.rank = parseInt(rank);
      }
      return ramen;
    })
    .filter(ramen => ramen.year === year)
    .sort((a, b) => a.rank > b.rank ? 1 : -1)

    this.setState(
      {
        ramens : tempStore,
        text: ''
      }
    )
  }

  filterByName(str){
    let tempStore = [...this.store];

    tempStore = tempStore.filter(ramen => {
      return ramen.Brand.toLowerCase().includes(str.toLowerCase())
    })

    this.setState(
      {
        ramens : tempStore,
        text : str
      }
    )
  }

  filterByCountry(str){
    let tempStore = [...this.store];

    tempStore = tempStore.filter(ramen => {
      return ramen.Country.toLowerCase() === str.toLowerCase()
    })

    this.setState(
      {
        ramens : tempStore,
        text : ''
      }
    )
  }

  render(){

    const WrapperStyle = {
      maxWidth: '600px',
      margin: '0 auto',
    }

    const containerStyle = {
      padding : "0 15px"
    }

    const HeadingStyle = {
      textAlign : 'center',
      fontSize : '30px',
      color: '#fff'
    }
    return (
      <div style={containerStyle}>
          <div style={WrapperStyle}>
            <h1 style={HeadingStyle}>Hungry for Ramen!</h1>
              <InputGroup 
                filterByName={this.filterByName} 
                filterByYear={this.filterByYear} 
                filterByCountry={this.filterByCountry} 
                text={this.state.text}
                countries={this.countries}/>
              {this.state.isFetched ? 
                this.state.ramens.map(ramen => <Ramen key={ramen.Variety} ramen={ramen}/>)
              : null}
          </div>
      </div>
    );
  }
}

export default App;
