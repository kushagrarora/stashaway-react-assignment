import React from 'react'
import PropTypes from 'prop-types'
import {throttle} from 'underscore';

function InputGroup(props) {

  const renderButtons = function(input){
    if(typeof input === 'number'){
      return <button 
              style={ButtonStyle}
              type="button"
              onClick={() => props.filterByYear(input)}>
                {input}
              </button>
    }
    else {
      return <button 
              style={ButtonStyle}
              key={input}
              type="button"
              onClick={() => props.filterByCountry(input)}>
                {input}
              </button>
    } 
  }

  const InputStyle = {
    width: '100%',
    border: '2px solid #eee',
    borderRadius: '4px',
    padding: '4px',
    fontSize: '20px',
    marginBottom: '16px',
  }

  const ButtonStyle = {
    padding : '4px 8px',
    border : '1px solid #eee',
    borderRadius : '4px',
    boxShadow : 'none',
    marginLeft : '4px',
    marginBottom: '4px',
    backgroundColor: '#eee'
  }

  const throttledInput = throttle(props.filterByName, 400)

  return (
    <div style={{color: '#fff'}}>
      <input 
        style={InputStyle}
        type="text" 
        placeholder="Search for restaurants"
        value={props.text}
        onChange={e => throttledInput(e.target.value)}
      />
      <div style={{marginBottom : '12px'}}>
        Top 10 of the year : 
        {renderButtons(2016)}
        {renderButtons(2015)}
        {renderButtons(2014)}
        {renderButtons(2013)}
        {renderButtons(2012)}
      </div>
      <div style={{display : 'flex'}}>
        <span style={{flexShrink : '0'}}>
          Search By Country :
        </span>
        <div>
          <div style={{marginBottom : '12px'}}>
          {props.countries.filter((a,b) => props.countries.indexOf(a) === b)
          .map(country => renderButtons(country))}
        </div>
        </div>
      </div>
    </div>
  )
}

InputGroup.propTypes = {
    filterByName : PropTypes.func,
    filterByYear : PropTypes.func,
    text : PropTypes.string
}

export default InputGroup

