import React from 'react'
import PropTypes from 'prop-types'
import image from '../media/ramen.jpg'
import halfStar from '../media/hstar.png'
import star from '../media/star.png'

function Ramen(props) {
    const { ramen } = props;

    const RamenStyle = {
        border : '1px solid #ccc',
        borderRadius : '4px',
        marginBottom : '16px',
        padding : '16px',
        display : 'flex',
        position : 'relative',
        backgroundColor: 'rgba(255,255,255,0.9)'
    }

    const ImageStyle = {
        width : '200px',
        position : 'relative',
        flexShrink : '0'
    }

    const MetaStyle = {
        paddingLeft : '16px',
        position : 'relative',
        flexGrow : '1'
    }

    const PackStyle = {
        position : 'absolute',
        bottom : '8px',
        right : '8px',
        backgroundColor : '#cccc00',
        color : '#fff',
        borderRadius : '4px',
        boxShadow : '0 0 2px #444',
        padding : '2px 4px',
        fontSize : '12px'
    }

    const RankStyle = {
        position : 'absolute',
        bottom : '16px',
        right: '16px',
        background: '#c8e7bd',
        borderRadius: '4px',
        padding: '4px 8px',
        fontSize: '10px',
        color: '#526b49',
    }

    const renderStars = function (count) {
        if(count === 'NaN')
            return
        else{
            let content = [];
            for (let i = 0; i < Math.floor(count); i++) {
                content.push(<img key={i} style={{height: '20px', width: '20px'}} src={star} alt=""/>)       
            }

            if (count%1 !== 0) {
                content.push(<img key={count} style={{height: '20px', width: '20px'}} src={halfStar} alt=""/>)
            }

            return <div style={{position : 'absolute', top : '0', right : '0'}}>
                {content}
            </div>
        }

    }

    return (
        <div style={RamenStyle}>
            <div style={ImageStyle}>
                <img src={image} alt={ramen.Brand} style={{maxWidth: '100%'}}/>
                {ramen.Style !== "Nan" && 
                    <div style={PackStyle}>
                        {ramen.Style}
                    </div>
                }
            </div>
            <div style={MetaStyle}>
                <h2 style={{margin : '0 0 8px'}}>{ramen.Variety}</h2>
                <div style={{margin : '0 0 8px', color : '#444', fontSize : '14px', position: 'relative'}}>
                    by {ramen.Brand} | {ramen.Country}
                    {renderStars(ramen.Stars)}
                </div>
            </div>
            {ramen["Top Ten"] !== 'NaN' && 
                <div style={RankStyle}>
                    <span style={{fontWeight: 'bold'}}>
                        {ramen["Top Ten"].split(' ')[1] + ' '}
                    </span>
                    in {ramen["Top Ten"].split(' ')[0]}
                </div>
            }
        </div>
    )
}

Ramen.propTypes = {
    ramen : PropTypes.object
}

export default Ramen

